import Task3.Validator;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class Tester {

    @Test
    public void test1() throws IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        assert (new Main().reflectionTask().equals("Ivan"));
    }

    @Test
    public void test2() {
        assert (new Main().proxyTask());
    }

    @Test
    public void test3() throws IllegalAccessException {
        assert (new Validator().validatorFunction());
    }

}
