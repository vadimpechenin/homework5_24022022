import Task1.ObjectCreater;
import Task1.Passport;
import Task1.Person;
import Task1.UserProfile;
import Task2.LogHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.*;

public class Main {
    public static void main(String[] args){

    }

    public static String reflectionTask() throws InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Person person = new Person("Ivan", "Ivanov", "qwerty");
        Passport passport = new Passport("2323", "2424242", new Date());
//        UserProfile userProfile = UserProfile.class.getConstructor(String.class, String.class, String.class, String.class).newInstance("Ivan", "Ivanov","2323", "2424242");

        UserProfile userProfile = ObjectCreater.mergeObjects(person, passport, UserProfile.class);
        System.out.println(userProfile.toString());
        return userProfile.getFirstName();
    }

    public static boolean proxyTask(){
/*         Map<String, Integer> loggedMap = (Map<String, Integer>) Proxy.newProxyInstance(
                        ClassLoader.getSystemClassLoader(),
                        new Class[]{Map.class},
                        (proxy, method, args1) -> {
                            if (method.getName().equals("put")){
                                return "hi!";
                            }else{
                                throw new UnsupportedOperationException("Unsupported method " + method.getName());
                            }
                }
                );

       System.out.println(loggedMap.put("1", 1));
        System.out.println(loggedMap.put("2", 2));*/

        Map<String, Integer> loggedMap2 = (Map<String, Integer>)
                        Proxy.newProxyInstance(
                        ClassLoader.getSystemClassLoader(),
                        new Class[]{Map.class},
                        new LogHandler(new HashMap<String, Integer>())
                );
        for (int i=0; i<10; i++) {
            loggedMap2.put(Integer.toString(i), i);
        }
        loggedMap2.get("2");
        Set<String> keys = loggedMap2.keySet();
        return true;
    }

}
