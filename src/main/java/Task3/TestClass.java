package Task3;

import java.util.List;

public class TestClass {

    @NotNull
    private List<Integer> numbers;

    @Min(21)
    @Max()
    private int age;

    @MaxLength
    @MinLength
    private String name;

    @NotEmpty
    private String surname;

    public TestClass(List<Integer> numbers, int number, String name, String surname){
        this.numbers = numbers;
        this.age = number;
        this.name = name;
        this.surname = surname;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestClass{" +
                "numbers=" + numbers +
                ", number=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
