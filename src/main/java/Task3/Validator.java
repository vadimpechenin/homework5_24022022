/*
Создать аннотации Min (числа), Max (числа), MaxLength (строки),
MinLength (строки), NotNull (ссылочные типы), NotEmpty (строки).
Реализовать класс Validator, который будет проверять POJO-объекты.
 */

package Task3;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

public class Validator {
    public static void main(String[] args) throws IllegalAccessException {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(3);
        int number = 50;
        String name = "James";
        String surname = "Bond";
        TestClass test = new TestClass(array, number,name,surname);
        check(test);

    }

    public static boolean validatorFunction() throws IllegalAccessException {
        ArrayList<Integer> array = new ArrayList<Integer>();
        array.add(3);
        int number = 50;
        String name = "James";
        String surname = "Bond";
        TestClass test = new TestClass(array, number,name,surname);

        System.out.println(test.toString());
        check(test);

        return true;
    }

    private static void check(Object o) throws IllegalAccessException {
        Class<?> clazz = o.getClass();
        for (Field field: clazz.getDeclaredFields()){
            if (field.isAnnotationPresent(MinLength.class)){
                MinLength annotation = field.getAnnotation(MinLength.class);
                int minLength = annotation.value();
                field.setAccessible(true);
                String stringField = (String) field.get(o);
                if (stringField.length()<minLength){
                    throw new RuntimeException("Длина строкового поля меньше чем требуется");
                }else{
                    System.out.println("Длина строкового поля больше нижней границы");
                }
            }

            if (field.isAnnotationPresent(MaxLength.class)){
                MaxLength annotation = field.getAnnotation(MaxLength.class);
                int maxLength = annotation.value();
                field.setAccessible(true);
                String stringField = (String) field.get(o);
                if (stringField.length()>maxLength){
                    throw new RuntimeException("Длина строкового поля больше чем требуется");
                }else{
                    System.out.println("Длина строкового поля меньше верхней границы");
                }
            }

            if (field.isAnnotationPresent(Max.class)){
                Max annotation = field.getAnnotation(Max.class);
                int max = annotation.value();
                field.setAccessible(true);
                int numberField = (Integer) field.get(o);
                if (numberField>max){
                    throw new RuntimeException("Величина возраста превышает максимальный");
                }else{
                    System.out.println("Возраст меньше верхней границы");
                }
            }

            if (field.isAnnotationPresent(Min.class)){
                Min annotation = field.getAnnotation(Min.class);
                int min = annotation.value();
                field.setAccessible(true);
                int numberField = (Integer) field.get(o);
                if (numberField<min){
                    throw new RuntimeException("Величина возраста ниже минимиальной границы");
                }else{
                    System.out.println("Возраст выше нижней границы");
                }
            }

            if (field.isAnnotationPresent(NotEmpty.class)){
                NotEmpty annotation = field.getAnnotation(NotEmpty.class);
                int notEmpty = annotation.value();
                field.setAccessible(true);
                String numberField = (String) field.get(o);
                if (numberField.length()==notEmpty){
                    throw new RuntimeException("Пустая строка");
                }else{
                    System.out.println("Строка не пустая");
                }
            }

            if (field.isAnnotationPresent(NotNull.class)){
                NotNull annotation = field.getAnnotation(NotNull.class);
                int notNull = annotation.value();
                field.setAccessible(true);
                ArrayList<Integer> numbersField = (ArrayList<Integer>) field.get(o);
                if (numbersField.size()==notNull){
                    throw new RuntimeException("Пустой массив");
                }else{
                    System.out.println("Массив не пустой");
                }
            }
        }

    }
}
