/*
Реализовать proxy класс который будет выводить в консоль имя метода
 и время его выполнения (проксировать интерфейс Map, использовать любую реализацию);
 */


package Task2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LogHandler implements InvocationHandler {
    private final Object delegate;

    public LogHandler(Object delegate) {
        this.delegate = delegate;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.printf("Method %s started\n", method.getName());
        double time = System.currentTimeMillis();
        Object result = method.invoke(delegate, args);
        double alltime = System.currentTimeMillis() - time;
        System.out.printf("Method %s finished. Runtime of method, ms: ", method.getName());
        System.out.printf("%.4f\n", alltime);
        return result;
    }
}
