package Task1;

import java.util.Date;

public class Passport {

    private String series;
    private String number;
    private Date dateDfIssue;

    public Passport(String series, String number, Date dateDfIssue) {
        this.series = series;
        this.number = number;
        this.dateDfIssue = dateDfIssue;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDateDfIssue() {
        return dateDfIssue;
    }

    public void setDateDfIssue(Date dateDfIssue) {
        this.dateDfIssue = dateDfIssue;
    }
}
