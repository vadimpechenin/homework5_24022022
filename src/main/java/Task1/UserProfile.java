package Task1;

public class UserProfile {

    private String firstName;
    private String lastName;
    private String series;
    private String number;

    public UserProfile(String firstName, String lastName, String series, String number) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.series = series;
        this.number = number;
    }

    public UserProfile() {
        /*this.firstName = "Ivan";
        this.lastName = "Ivanov";
        this.series = "2323";
        this.number = "2424242";*/
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", series='" + series + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
