package Task1;/*
Класс, метод которого будет копировать
значения полей класса A и класса B
в новый класс C, если их имена совпадают;


 */
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

public class ObjectCreater {

    public static <T> T mergeObjects(Object firstObject, Object secondObject, Class<T> tClass) throws IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Class<?> firstObjectClass = firstObject.getClass();
        Class<?> secondObjectClass = secondObject.getClass();

        Field[] fieldsFirst = firstObjectClass.getDeclaredFields();
        Field[] fieldsSecond = secondObjectClass.getDeclaredFields();
        Field[] fieldsThird = tClass.getDeclaredFields();

        String nameFirst;
        String nameSecond;
        String nameThird;

        T thirdObject = tClass.newInstance();
        // Пройтись по полям, найти совпадения и записать для вызова названий и значений
        for (int i=0; i<fieldsThird.length; i++){
            nameThird = fieldsThird[i].getName();
            for (int ij=0; ij<fieldsFirst.length; ij++){
                nameFirst = fieldsFirst[ij].getName();
                if (nameThird.equals(nameFirst)){
                    changeField(thirdObject,nameFirst,getFieldValue(firstObject, nameFirst));
                    break;
                }
            }
            for (int ij=0; ij<fieldsSecond.length; ij++){
                nameSecond = fieldsSecond[ij].getName();
                if (nameThird.equals(nameSecond)){
                    changeField(thirdObject,nameSecond,getFieldValue(secondObject, nameSecond));
                    break;
                }
            }

        }


        return thirdObject;
    }


    private static <T> T getFieldValue(Object object, String fieldName) throws IllegalAccessException, NoSuchFieldException {
        Class <?> objectClass = object.getClass();
        Field field = objectClass.getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T) field.get(object);
    }

    public static void changeField(Object object, String fieldName, Object fieldValue) throws NoSuchFieldException, IllegalAccessException {
        Class<?> exClass = object.getClass();
        Field field = exClass.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(object,fieldValue);
    }
}
